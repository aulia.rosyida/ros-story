from django.db import models
from django.utils import timezone 
from datetime import datetime, date

class Schedule(models.Model):    
    date_time = models.DateTimeField(default=timezone.now)   
    activity  = models.CharField(max_length=100)
    place     = models.CharField(max_length=100)
    category  = models.CharField(max_length=100)