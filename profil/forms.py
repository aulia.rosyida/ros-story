from django import forms
from .models import Schedule
from django.forms import ModelForm

class Schedule_Form(forms.ModelForm):

    class Meta:
        model = Schedule
        fields = ('date_time', 'activity', 'place', 'category', )
