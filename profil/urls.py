from django.urls import path
from profil import views

urlpatterns = [
    path('', views.cover, name='cover'),
    path('profil/', views.profil, name='profil'),
    path('guestForm/', views.guest, name='guest'),
    path('schedule/', views.sched, name='sched'),
    path('schedule/Delete', views.schedDel, name='hapus' )
]