from django.shortcuts import render
from .forms import Schedule_Form
from django.utils import timezone 
from .models import Schedule
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.
response = {}
def profil(request): 
    return render(request, 'story4.html', {})
    
def cover(request): 
    return render(request, 'cover.html', {})

def guest(request): 
    return render(request, 'guestForm.html', {})

def sched(request):
    events = Schedule.objects.all().values() 
    if request.method == "POST":
        form = Schedule_Form(request.POST)
        if form.is_valid():
            # response['date_time'] = request.POST['date_time']
            # response['activity']  = request.POST['activity']
            # response['place']     = request.POST['place'] , 
            # response['category']  = request.POST['category']
            # post = Schedule(date_time=response['date_time'], activity=response['activity'], place=response['place'], category=response['category'] )
            form.save()
            return HttpResponseRedirect(reverse('sched'))
    else: 
        form = Schedule_Form() 
    response = {'form': form, 'kegiatan':events}    
    return render(request, 'scheduleForm.html',response)

def schedDel(request):
    Schedule.objects.all().delete() 
    return HttpResponseRedirect(reverse("sched"))






