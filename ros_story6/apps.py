from django.apps import AppConfig


class RosStory6Config(AppConfig):
    name = 'ros_story6'
