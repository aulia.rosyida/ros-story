from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import add_status   #index, 
from .models import Status
from .forms import Status_Form
    
    # Create your tests here.
    class ros_story6UnitTest(TestCase):
    
        def test_story_6_url_is_exist(self):
            response = Client().get('/story-6/')
            self.assertEqual(response.status_code, 200)
    
        def test_story6_using_add_status_func(self):
            found = resolve('/story-6/')
            self.assertEqual(found.func, add_status)
    
        def test_model_can_create_new_status(self):
            # Creating a new status
            new_status = Status.objects.create(time='2018-10-10 00:00:00' , status='mengerjakan lab_5 ppw')
    
            # Retrieving all status
            counting_all_available_status = Status.objects.all().count()
            self.assertEqual(counting_all_available_status, 1)
    
        # def test_form_status_input_has_placeholder_and_css_classes(self):
        #     form = Todo_Form()
        #     self.assertIn('class="todo-form-input', form.as_p())
        #     self.assertIn('id="id_title"', form.as_p())
        #     self.assertIn('class="todo-form-textarea', form.as_p())
        #     self.assertIn('id="id_description', form.as_p())
    
        def test_status_validation_for_blank_items(self):
            form = Status_Form(data={'time': '', 'status': ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['status'],
                ["This field is required."]
            )
        def test_story6_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/story-6/', {'time': test, 'status': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/story-6/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)
    
        def test_story6_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/story-6/', {'time': '', 'status': ''})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/story-6/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)

